import React from 'react';
import { Link } from 'react-router-dom';

function SeriesColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        const series = data.series;
        return (
          <div key={series.href} className="card mb-3 shadow">
            <img src={series.location.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{series.name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {series.location.name}
              </h6>
              <p className="card-text">
                {series.description}
              </p>
            </div>
            <div className="card-footer">
              {new Date(series.starts).toLocaleDateString()}
              -
              {new Date(series.ends).toLocaleDateString()}
            </div>
          </div>
        );
      })}
    </div>
  );
}

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      seriesColumns: [[], [], []],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/seriess/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of seriess
        const data = await response.json();

        // Create a list of for all the requests and
        // add all of the requests to it
        const requests = [];
        for (let series of data.seriess) {
          const detailUrl = `http://localhost:8000${series.href}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish
        // simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the series
        // information into
        const seriesColumns = [[], [], []];

        // Loop over the series detail responses and add
        // each to to the proper "column" if the response is
        // ok
        let i = 0;
        for (const seriesResponse of responses) {
          if (seriesResponse.ok) {
            const details = await seriesResponse.json();
            seriesColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(seriesResponse);
          }
        }

        // Set the state to the new list of three lists of
        // seriess
        this.setState({seriesColumns: seriesColumns});
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
      <img className="bg-white rounded shadow d-block mx-auto mb-4" src="logo.jpg" alt="" width="600" />
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <h1 className="display-5 fw-bold">Anime DB</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              A cruel angel's thesis.
            </p>
          </div>
        </div>
        <div className="container">
          <h2>Most popular seriess</h2>
          <div className="row">
            {this.state.seriesColumns.map((seriesList, index) => {
              return (
                <SeriesColumn key={index} list={seriesList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default MainPage;
