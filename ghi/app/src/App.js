import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import SeriesForm from "./SeriesForm";
import MainPage from "./MainPage";

function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="series">
          <Route path="new" element={<SeriesForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
