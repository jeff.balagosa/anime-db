import { useEffect, useState } from 'react';

function SeriesList() {
  const [series, setSeries] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8001/api/series/');

    if (response.ok) {
      const data = await response.json();
      setSeries(data.series)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {attendees.map(attendee => {
          return (
            <tr key={series.href}>
              <td>{ series.name }</td>
              <td>{ series.season_count }</td>
              <td>{ series.episode_count }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AttendeesList;
