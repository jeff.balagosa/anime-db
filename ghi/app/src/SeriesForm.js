import React, { useState, useEffect } from "react";

function SeriesForm() {
  // Call useState to create a variable and a function to update that variable
  const [name, setName] = useState([]);
  const [seasons, setSeasons] = useState([]);
  const [episodes, setEpisodes] = useState([]);
  const [synopsis, setSynopsis] = useState([]);

  // Create a callback function that updates name variable when input changes (i.e. user types)
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };
  const handleSeasonChange = (event) => {
    const value = event.target.value;
    setSeasons(value);
  };
  const handleEpisodeChange = (event) => {
    const value = event.target.value;
    setEpisodes(value);
  };
  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  };

  return (
    <div>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            Series Name: <b>{name}</b>
            <br /># Seasons: <b>{seasons}</b>
            <br /># Episodes: <b>{episodes}</b>
            <br />
            Synopsis: <b>{synopsis}</b>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new series</h1>
            <form id="create-series-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              {/* Seasons Input */}
              <div className="form-floating mb-3">
                <input
                  onChange={handleSeasonChange}
                  placeholder="# of Seasons"
                  required
                  type="number"
                  name="seasons"
                  id="seasons"
                  className="form-control"
                />
                <label htmlFor="seasons"># Seasons</label>
              </div>
              {/* Episodes Input */}
              <div className="form-floating mb-3">
                <input
                  onChange={handleEpisodeChange}
                  placeholder="# of Episodes"
                  required
                  type="number"
                  name="episodes"
                  id="episodes"
                  className="form-control"
                />
                <label htmlFor="episodes"># Episodes</label>
              </div>
              {/* Synopsis Input */}
              <div className="form-floating mb-3">
                <textarea
                  onChange={handleSynopsisChange}
                  placeholder="Synopsis"
                  required
                  name="synopsis"
                  id="synopsis"
                  className="form-control"
                ></textarea>
                <label htmlFor="synopsis">Synopsis</label>
              </div>
              <div className="mb-3"></div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SeriesForm;
